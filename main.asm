section .rodata

    %include "words.inc"
    no_input_message: db "Your input is too long", 10, 0
    no_such_entry_message: db "No entry with such key in the dictionary", 10, 0
    no_dictionary_message: db "The dictionary is empty", 10, 0

section .bss

    buf: resb 256

section .text

extern exit
extern find_word
extern read_word
extern string_length
extern print_string
extern print_newline
extern find_word

global _start

_start:
    %ifndef HEAD_PTR
        jmp .no_dict ; fail if the dictionary is empty
    %else
        mov rdi, buf
        mov rsi, 256
        call read_word
	test rax, rax
        jz .no_input ; read key, fail if the key doesn't fit into the buffer
	mov rdi, buf
        mov rsi, HEAD_PTR
        call find_word
        test rax, rax
        jz .no_such_entry ; fail if the entry with this key isn't found
        mov rdi, rax
        call print_string
        call print_newline ; print the value from the dictionary
        call exit
    %endif
    .no_dict:
        mov rdi, no_dictionary_message
	call print_error
	call exit
    .no_input:
        mov rdi, no_input_message
	call print_error
	call exit
    .no_such_entry:
        mov rdi, no_such_entry_message
	call print_error
	call exit


print_error:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rdi, 2
    mov rax, 1
    syscall
    ret
