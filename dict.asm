%define PTR_SIZE 8

section .text

extern string_equals
extern string_length

global find_word

find_word:
    .loop:
        push rdi
        push rsi
	add rsi, PTR_SIZE ; get key address
        call string_equals ; check if the key matches
        pop rsi
        pop rdi
        test rax, rax
        jnz .success
        mov rsi, [rsi]
	test rsi, rsi 
	jz .fail ; fail if the end of the dictionary has been reached
	jmp .loop
    .fail:
        xor rax, rax
	ret
    .success:
	push rdi
	push rsi
	call string_length
        pop rsi
	pop rdi
	add rsi, PTR_SIZE
	add rsi, rax ; adding string length
	inc rsi ; adding 1 for null terminator
	mov rax, rsi
	ret
