ASM =  nasm
ASMFLAGS = -felf64
LD = ld

all: main clean
.PHONY: all clean

main: lib.o dict.o main.o
	$(LD) -o $@ $^

main.o: main.asm words.inc colon.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

clean:
	rm *.o

