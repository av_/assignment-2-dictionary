%macro colon 2
    %ifndef HEAD_PTR
        %define HEAD_PTR 0
    %endif
    %2:
        dq HEAD_PTR
	db %1, 0
    %define HEAD_PTR %2

%endmacro
